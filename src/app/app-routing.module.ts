import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
   { path: '', loadChildren: () => import('./modules/bmi/bmi.module').then(m => m.BmiModule)},
   { path: 'bmi', loadChildren: () => import('./modules/bmi/bmi.module').then(m => m.BmiModule) },
   { path: 'calories', loadChildren: () => import('./modules/calories/calories.module').then(m => m.CaloriesModule) },
   { path: 'water', loadChildren: () => import('./modules/water/water.module').then(m => m.WaterModule) },
   { path: '*', redirectTo:'/bmi' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
