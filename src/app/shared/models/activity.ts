export class CaloricDemand {
    public bmr: string = '0';
    public cpm: string = '0';
}

export interface IActivityHeaviness {
  id: number;
  heaviness: string;
  multiply: number;
}

export const activitysHeaviness: IActivityHeaviness[] = [
  { id: 1, heaviness: "Bardzo wysoka aktywność", multiply: 2.0 },
  { id: 2, heaviness: "Wysoka aktywność", multiply: 1.7 },
  { id: 3, heaviness: "Średnia aktywność", multiply: 1.5 },
  { id: 4, heaviness: "Mała aktywność", multiply: 1.3 },  
  { id: 5, heaviness: "Brak aktywności", multiply: 1.2}
];
