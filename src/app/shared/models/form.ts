export type IGender = 'man' | 'woman';

export interface IForm {
    weight: number;
    growth?: number;
}

export interface IFormCalories extends IForm {
    gender: IGender;
    activitys: number;
    age: number;
}

