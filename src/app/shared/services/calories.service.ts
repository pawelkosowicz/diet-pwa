import { Injectable } from '@angular/core';
import { IFormCalories } from '../models/form';
import { CaloricDemand } from '../models/activity';

@Injectable({
  providedIn: 'root'
})
export class CaloriesService {

  constructor() { }
  
  public onCaloriesCalculate({weight,growth,activitys,age,gender}: IFormCalories): CaloricDemand {
    const calories: CaloricDemand = new CaloricDemand();
    if(gender === 'man') {
      calories.bmr = ((66 + (13.7*weight) + (5*growth) - (6.76 * age))).toFixed(0);      
    } else {
      calories.bmr = ((655 + (9.6*weight) + (1.8*growth) - (4.7 * age))).toFixed(0);
    }

    calories.cpm = (Number.parseFloat(calories.bmr) * activitys).toFixed(0);
    return calories;
  }

}
