import { Injectable } from '@angular/core';
import { IForm } from '../models/form';

@Injectable({
  providedIn: 'root'
})
export class WaterService {

  constructor() { }

  public onWaterCalculate({weight}: IForm): number {
    return Number.parseInt((weight * 35).toFixed(0),10)
  }
}
