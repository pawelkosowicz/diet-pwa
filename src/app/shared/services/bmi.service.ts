import { Injectable } from '@angular/core';
import {  IBmiType, bmiTypeArray } from '../types/BMITypes';
import { IForm } from '../models/form';

@Injectable({
  providedIn: 'root'
})
export class BmiService { 

  constructor() { }

  public calculateBMI(bmiFormValue: IForm): IBmiType {
    const {weight,growth} = bmiFormValue;
    const bmi: number = (weight/((growth/100)^2));
    const bmiType: IBmiType = bmiTypeArray.find(({weight}: IBmiType)=> weight.from<=bmi && weight.to>=bmi);
    return {
      ...bmiType,
      bmi: bmi.toFixed(2)
    };
  }
  
}
