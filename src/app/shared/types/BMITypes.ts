export interface IBmiType {
    weight: IBmiWeightInterval;
    description: string;
    bmi?: string;
}

export interface IBmiWeightInterval {
    from: number;
    to: number;
}

export const bmiTypeArray: IBmiType[] = [
    {weight: {from: 0, to: 18.5}, description: 'niedowaga'},
    {weight: {from: 18.5, to: 24.9}, description: 'prawidłowa'},
    {weight: {from: 25.0, to: 29.9}, description: 'nadwaga'},
    {weight: {from: 30.0, to: 34.9}, description: 'otyłość I stopnia'},
    {weight: {from: 35.0, to: 39.9}, description: 'otyłość II stopnia'},
    {weight: {from: 40, to: 9999}, description: 'otyłość II stopnia'}
]