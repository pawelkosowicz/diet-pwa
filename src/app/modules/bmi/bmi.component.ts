import { Component, OnInit } from '@angular/core';
import { BmiService } from 'src/app/shared/services/bmi.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IBmiType } from 'src/app/shared/types/BMITypes';
import { IForm } from 'src/app/shared/models/form';

@Component({
  selector: 'app-bmi',
  templateUrl: './bmi.component.html',
  styleUrls: ['./bmi.component.scss']
})
export class BmiComponent implements OnInit {

  public bmiType: IBmiType;
  public bmiForm: FormGroup;

  constructor(private bmiSrv: BmiService,
    private formBuilder: FormBuilder) {
    this.bmiType = undefined;

    this.bmiForm = this.formBuilder.group({
      weight: ['',[Validators.min(0),Validators.max(10000), Validators.required]],
      growth: ['',[Validators.min(0),Validators.max(10000), Validators.required]]
    })
  }

  ngOnInit(): void {
  }

  public onBmiCalculate(bmiFormValue: IForm) {
    this.bmiType = this.bmiSrv.calculateBMI(bmiFormValue);   
  }

}
