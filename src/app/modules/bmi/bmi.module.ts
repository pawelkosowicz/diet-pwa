import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BmiComponent } from './bmi.component';
import { BmiRoutingModule } from './bmi-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [BmiComponent],
  imports: [
    CommonModule,
    BmiRoutingModule,
    SharedModule   
  ]
})
export class BmiModule { }
