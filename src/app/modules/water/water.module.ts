import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WaterComponent } from './water.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { WaterRoutingModule } from './water-routing.module';

@NgModule({
  declarations: [WaterComponent],
  imports: [
    CommonModule,
    SharedModule,
    WaterRoutingModule
  ]
})
export class WaterModule { }
