import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WaterService } from 'src/app/shared/services/water.service';
import { IForm } from 'src/app/shared/models/form';

@Component({
  selector: 'app-water',
  templateUrl: './water.component.html',
  styleUrls: ['./water.component.scss']
})
export class WaterComponent implements OnInit {

  public waterForm: FormGroup;
  public waterDemand: number;

  constructor(private formBuilder: FormBuilder,
    private waterService: WaterService) { 
    this.waterDemand = undefined;
    this.waterForm = this.formBuilder.group({
       weight: ['',[Validators.required,Validators.min(0),Validators.max(10000)]] 
    })
  }

  ngOnInit(): void {
  }

  public onWaterCalculate(waterFormValue: IForm) {
    this.waterDemand = this.waterService.onWaterCalculate(waterFormValue);
  }

}
