import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { activitysHeaviness, IActivityHeaviness, CaloricDemand } from 'src/app/shared/models/activity';
import { IFormCalories } from 'src/app/shared/models/form';
import { CaloriesService } from 'src/app/shared/services/calories.service';

@Component({
  selector: 'app-calories',
  templateUrl: './calories.component.html',
  styleUrls: ['./calories.component.scss']
})
export class CaloriesComponent implements OnInit {

  public caloriesForm: FormGroup;
  public activitysHeaviness: IActivityHeaviness[];
  public caloricDemand: CaloricDemand;

  constructor(private formBuilder: FormBuilder,
    private caloriesSerivce: CaloriesService) {
    this.caloricDemand = undefined;
    this.activitysHeaviness =  activitysHeaviness;

    this.caloriesForm = this.formBuilder.group({
      gender: ['',[Validators.required]],
      age: ['',[Validators.min(0),Validators.max(10000), Validators.required]],
      weight: ['', [Validators.min(0),Validators.max(10000), Validators.required]],
      growth: ['',[Validators.min(0),Validators.max(10000), Validators.required]],
      activitys: [this.activitysHeaviness[0].multiply]
    })    
  }

  ngOnInit(): void {
  }

  public onCaloriesCalculate(caloriesFormValue: IFormCalories) {
    this.caloricDemand = this.caloriesSerivce.onCaloriesCalculate(caloriesFormValue);
  }

}
