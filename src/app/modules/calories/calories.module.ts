import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaloriesComponent } from './calories.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { CaloriesRoutingModule } from './calories-routing.module';

@NgModule({
  declarations: [CaloriesComponent],
  imports: [
    CommonModule,
    CaloriesRoutingModule,
    SharedModule
  ]
})
export class CaloriesModule { }
