import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CaloriesComponent } from './calories.component';

const routes: Routes = [
    {path: '',component: CaloriesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CaloriesRoutingModule { }
